# Small program to read a genotype file and compute a genomic relationship matrix

## Aim
The aim of this project was to illustrate the use of [the Fortran Package Manager
`fpm`](https://github.com/fortran-lang/fpm) and the [Fortran Standard
Library](https://github.com/fortran-lang/stdlib) for computing a genomic
relationship matrix.

## Instructions

The compilation of this small project can be done through `fpm` as follows:
```shell
fpm build
```

To run the app, it can be done as follows:
```shell
fpm run
```
