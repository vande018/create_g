module create_g
  implicit none
  private

  public :: say_hello
contains
  subroutine say_hello
    print *, "Hello, create_g!"
  end subroutine say_hello
end module create_g
