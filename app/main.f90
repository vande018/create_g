program create_g
  use stdlib_io, only: loadtxt, savetxt
  use stdlib_stats, only: mean
  use stdlib_array, only: trueloc
  implicit none
  integer, allocatable :: qc_maf(:), geno(:,:)
  real, allocatable :: freq(:), Z(:, :), G(:, :)

  call loadtxt('geno.txt', geno)

  freq = mean(geno, 1) / 2

  qc_maf = trueloc(freq > 0.05 .and. freq < 0.95)

  geno = geno(:, qc_maf)
  freq = freq(qc_maf)

  Z = geno - spread(2 * freq, dim = 1, ncopies = size(geno, 1))

  G = matmul(Z, transpose(Z)) / (2 * dot_product(freq, 1-freq))

  call savetxt('G.txt', G)

end program create_g
